import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

import { ethers } from "hardhat";
import { expect } from "chai";
import { BigNumber, ContractReceipt } from "ethers";
import { ecsign, ecrecover, pubToAddress, bufferToHex } from "ethereumjs-util";

import {
  ContractRegistry,
  DEXParameters,
  DEXParameters__factory,
  ERC20Periphery,
  UniswapV2Factory,
  UniswapV2Pair,
  UniswapV2Router02,
  WQ,
} from "../../typechain";
import { getApprovalDigest, MINIMUM_LIQUIDITY } from "./shared/utilities";

const overrides = {
  gasLimit: 9999999,
};

const FLAT_FEE = ethers.utils.parseEther("0.5");
const FLAT_FEE_KEY = "defi.QDEX.flatFee";
const DEX_PARAMETERS_KEY = "governance.experts.EPDR.QDEX.parameters";
const DEX_PARAMETERS_VOTING_KEY =
  "governance.experts.EPDR.QDEX.parametersVoting";
const REVENUE_POOL_KEY = "governance.intApp.QDEX.revenuePool";
const ROUTER_KEY = "governance.intApp.QDEX.router";

const mineBlock = async (secondsInFuture: number) => {
  const latestBlock = await ethers.provider.getBlock("latest");
  await ethers.provider.send("evm_mine", [
    latestBlock.timestamp + secondsInFuture,
  ]);
};

describe("UniswapV2Router02", () => {
  let wallet: SignerWithAddress, other: SignerWithAddress;
  // run "npx hardhat node" and copy the first private key (if it doesn't match this one)
  const privateKey =
    "ac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80";

  let WQ: WQ;
  let WQPartner: ERC20Periphery;
  let token0: ERC20Periphery;
  let token1: ERC20Periphery;
  let factory: UniswapV2Factory;
  let pair: UniswapV2Pair;
  let WQPair: UniswapV2Pair;

  let registry: ContractRegistry;
  let router: UniswapV2Router02;
  let dexParameters: DEXParameters;

  beforeEach(async () => {
    [wallet, other] = await ethers.getSigners();

    const registryFactory = await ethers.getContractFactory("ContractRegistry");
    registry = await registryFactory.deploy();

    const dexParametersFactory = (await ethers.getContractFactory(
      "DEX_Parameters"
    )) as DEXParameters__factory;
    dexParameters = await dexParametersFactory.deploy();
    await dexParameters.initialize(
      registry.address,
      [FLAT_FEE_KEY],
      [FLAT_FEE],
      [],
      [],
      [],
      [],
      [],
      []
    );

    const Factory = await ethers.getContractFactory("UniswapV2Factory");
    factory = await Factory.deploy(registry.address);

    let tokenFactory = await ethers.getContractFactory("ERC20Periphery");
    token0 = await tokenFactory.deploy(ethers.utils.parseEther("10000"));
    token1 = await tokenFactory.deploy(ethers.utils.parseEther("10000"));
    WQPartner = await tokenFactory.deploy(ethers.utils.parseEther("10000"));

    if (token0.address.toLowerCase() > token1.address.toLowerCase())
      [token0, token1] = [token1, token0];

    const WQFactory = await ethers.getContractFactory("WQ");
    WQ = await WQFactory.deploy();

    await factory.createPair(token0.address, token1.address);
    let pairAddress = await factory.getPair(token0.address, token1.address);
    pair = await ethers.getContractAt("UniswapV2Pair", pairAddress);

    await factory.createPair(WQ.address, WQPartner.address);
    pairAddress = await factory.getPair(WQ.address, WQPartner.address);
    WQPair = await ethers.getContractAt("UniswapV2Pair", pairAddress);

    let routerFactory = await ethers.getContractFactory("UniswapV2Router02");
    router = await routerFactory.deploy(
      factory.address,
      WQ.address,
      registry.address
    );

    await registry.initialize(
      [wallet.address],
      [
        ROUTER_KEY,
        DEX_PARAMETERS_KEY,
        REVENUE_POOL_KEY,
        DEX_PARAMETERS_VOTING_KEY,
      ],
      [router.address, dexParameters.address, wallet.address, wallet.address]
    );
  });

  it("addLiquidity", async () => {
    const token0Amount = ethers.utils.parseEther("1");
    const token1Amount = ethers.utils.parseEther("4");

    const expectedLiquidity = ethers.utils.parseEther("2");
    await token0.approve(router.address, ethers.constants.MaxUint256);
    await token1.approve(router.address, ethers.constants.MaxUint256);

    await expect(
      router.addLiquidity(
        token0.address,
        token1.address,
        token0Amount,
        token1Amount,
        0,
        0,
        wallet.address,
        ethers.constants.MaxUint256,
        overrides
      )
    )
      .to.emit(token0, "Transfer")
      .withArgs(wallet.address, pair.address, token0Amount)
      .to.emit(token1, "Transfer")
      .withArgs(wallet.address, pair.address, token1Amount)
      .to.emit(pair, "Transfer")
      .withArgs(
        ethers.constants.AddressZero,
        ethers.constants.AddressZero,
        MINIMUM_LIQUIDITY
      )
      .to.emit(pair, "Transfer")
      .withArgs(
        ethers.constants.AddressZero,
        wallet.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(pair, "Sync")
      .withArgs(token0Amount, token1Amount)
      .to.emit(pair, "Mint")
      .withArgs(router.address, token0Amount, token1Amount);

    expect(await pair.balanceOf(wallet.address)).to.eq(
      expectedLiquidity.sub(MINIMUM_LIQUIDITY)
    );
  });

  it("addLiquidityETH", async () => {
    const WQPartnerAmount = ethers.utils.parseEther("1");
    const QAmount = ethers.utils.parseEther("4");

    const expectedLiquidity = ethers.utils.parseEther("2");
    const WQPairToken0 = await WQPair.token0();
    await WQPartner.approve(router.address, ethers.constants.MaxUint256);
    await expect(
      router.addLiquidityETH(
        WQPartner.address,
        WQPartnerAmount,
        WQPartnerAmount,
        QAmount,
        wallet.address,
        ethers.constants.MaxUint256,
        { ...overrides, value: QAmount }
      )
    )
      .to.emit(WQPair, "Transfer")
      .withArgs(
        ethers.constants.AddressZero,
        ethers.constants.AddressZero,
        MINIMUM_LIQUIDITY
      )
      .to.emit(WQPair, "Transfer")
      .withArgs(
        ethers.constants.AddressZero,
        wallet.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(WQPair, "Sync")
      .withArgs(
        WQPairToken0 === WQPartner.address ? WQPartnerAmount : QAmount,
        WQPairToken0 === WQPartner.address ? QAmount : WQPartnerAmount
      )
      .to.emit(WQPair, "Mint")
      .withArgs(
        router.address,
        WQPairToken0 === WQPartner.address ? WQPartnerAmount : QAmount,
        WQPairToken0 === WQPartner.address ? QAmount : WQPartnerAmount
      );

    expect(await WQPair.balanceOf(wallet.address)).to.eq(
      expectedLiquidity.sub(MINIMUM_LIQUIDITY)
    );
  });

  async function addLiquidity(
    token0Amount: BigNumber,
    token1Amount: BigNumber
  ) {
    await token0.transfer(pair.address, token0Amount);
    await token1.transfer(pair.address, token1Amount);

    // override the onlyRouter constraint in order to mint tokens
    await registry.setAddress(ROUTER_KEY, wallet.address);
    await pair.mint(wallet.address, overrides);
    await registry.setAddress(ROUTER_KEY, router.address);
  }

  it("removeLiquidity", async () => {
    const token0Amount = ethers.utils.parseEther("1");
    const token1Amount = ethers.utils.parseEther("4");
    await addLiquidity(token0Amount, token1Amount);

    const expectedLiquidity = ethers.utils.parseEther("2");
    await pair.approve(router.address, ethers.constants.MaxUint256);
    await expect(
      router.removeLiquidity(
        token0.address,
        token1.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY),
        0,
        0,
        wallet.address,
        ethers.constants.MaxUint256,
        overrides
      )
    )
      .to.emit(pair, "Transfer")
      .withArgs(
        wallet.address,
        pair.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(pair, "Transfer")
      .withArgs(
        pair.address,
        ethers.constants.AddressZero,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(token0, "Transfer")
      .withArgs(pair.address, wallet.address, token0Amount.sub(500))
      .to.emit(token1, "Transfer")
      .withArgs(pair.address, wallet.address, token1Amount.sub(2000))
      .to.emit(pair, "Sync")
      .withArgs(500, 2000)
      .to.emit(pair, "Burn")
      .withArgs(
        router.address,
        token0Amount.sub(500),
        token1Amount.sub(2000),
        wallet.address
      );

    expect(await pair.balanceOf(wallet.address)).to.eq(0);
    const totalSupplyToken0 = await token0.totalSupply();
    const totalSupplyToken1 = await token1.totalSupply();
    expect(await token0.balanceOf(wallet.address)).to.eq(
      totalSupplyToken0.sub(500)
    );
    expect(await token1.balanceOf(wallet.address)).to.eq(
      totalSupplyToken1.sub(2000)
    );
  });

  it("removeLiquidityETH", async () => {
    const WQPartnerAmount = ethers.utils.parseEther("1");
    const QAmount = ethers.utils.parseEther("4");
    await WQPartner.transfer(WQPair.address, WQPartnerAmount);
    await WQ.deposit({ value: QAmount });
    await WQ.transfer(WQPair.address, QAmount);

    // override the onlyRouter constraint in order to mint tokens
    await registry.setAddress(ROUTER_KEY, wallet.address);
    await WQPair.mint(wallet.address, overrides);
    await registry.setAddress(ROUTER_KEY, router.address);

    const expectedLiquidity = ethers.utils.parseEther("2");
    const WQPairToken0 = await WQPair.token0();
    await WQPair.approve(router.address, ethers.constants.MaxUint256);
    await expect(
      router.removeLiquidityETH(
        WQPartner.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY),
        0,
        0,
        wallet.address,
        ethers.constants.MaxUint256,
        overrides
      )
    )
      .to.emit(WQPair, "Transfer")
      .withArgs(
        wallet.address,
        WQPair.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(WQPair, "Transfer")
      .withArgs(
        WQPair.address,
        ethers.constants.AddressZero,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY)
      )
      .to.emit(WQ, "Transfer")
      .withArgs(WQPair.address, router.address, QAmount.sub(2000))
      .to.emit(WQPartner, "Transfer")
      .withArgs(WQPair.address, router.address, WQPartnerAmount.sub(500))
      .to.emit(WQPartner, "Transfer")
      .withArgs(router.address, wallet.address, WQPartnerAmount.sub(500))
      .to.emit(WQPair, "Sync")
      .withArgs(
        WQPairToken0 === WQPartner.address ? 500 : 2000,
        WQPairToken0 === WQPartner.address ? 2000 : 500
      )
      .to.emit(WQPair, "Burn")
      .withArgs(
        router.address,
        WQPairToken0 === WQPartner.address
          ? WQPartnerAmount.sub(500)
          : QAmount.sub(2000),
        WQPairToken0 === WQPartner.address
          ? QAmount.sub(2000)
          : WQPartnerAmount.sub(500),
        router.address
      );

    expect(await WQPair.balanceOf(wallet.address)).to.eq(0);
    const totalSupplyWQPartner = await WQPartner.totalSupply();
    const totalSupplyWQ = await WQ.totalSupply();
    expect(await WQPartner.balanceOf(wallet.address)).to.eq(
      totalSupplyWQPartner.sub(500)
    );
    expect(await WQ.balanceOf(wallet.address)).to.eq(totalSupplyWQ.sub(2000));
  });

  it("removeLiquidityWithPermit", async () => {
    const token0Amount = ethers.utils.parseEther("1");
    const token1Amount = ethers.utils.parseEther("4");
    await addLiquidity(token0Amount, token1Amount);

    const expectedLiquidity = ethers.utils.parseEther("2");

    const nonce = await pair.nonces(wallet.address);
    const digest = await getApprovalDigest(
      pair,
      {
        owner: wallet.address,
        spender: router.address,
        value: expectedLiquidity.sub(MINIMUM_LIQUIDITY),
      },
      nonce,
      ethers.constants.MaxUint256
    );

    const { v, r, s } = ecsign(
      Buffer.from(digest.slice(2), "hex"),
      Buffer.from(privateKey, "hex")
    );

    await router.removeLiquidityWithPermit(
      token0.address,
      token1.address,
      expectedLiquidity.sub(MINIMUM_LIQUIDITY),
      0,
      0,
      wallet.address,
      ethers.constants.MaxUint256,
      false,
      v,
      r,
      s,
      overrides
    );
  });

  it("removeLiquidityETHWithPermit", async () => {
    const WQPartnerAmount = ethers.utils.parseEther("1");
    const QAmount = ethers.utils.parseEther("4");
    await WQPartner.transfer(WQPair.address, WQPartnerAmount);
    await WQ.deposit({ value: QAmount });
    await WQ.transfer(WQPair.address, QAmount);

    // override the onlyRouter constraint in order to mint tokens
    await registry.setAddress(ROUTER_KEY, wallet.address);
    await WQPair.mint(wallet.address, overrides);
    await registry.setAddress(ROUTER_KEY, router.address);

    const expectedLiquidity = ethers.utils.parseEther("2");

    const nonce = await WQPair.nonces(wallet.address);
    const digest = await getApprovalDigest(
      WQPair,
      {
        owner: wallet.address,
        spender: router.address,
        value: expectedLiquidity.sub(MINIMUM_LIQUIDITY),
      },
      nonce,
      ethers.constants.MaxUint256
    );

    const { v, r, s } = ecsign(
      Buffer.from(digest.slice(2), "hex"),
      Buffer.from(privateKey, "hex")
    );

    await router.removeLiquidityETHWithPermit(
      WQPartner.address,
      expectedLiquidity.sub(MINIMUM_LIQUIDITY),
      0,
      0,
      wallet.address,
      ethers.constants.MaxUint256,
      false,
      v,
      r,
      s,
      overrides
    );
  });

  describe("swapExactTokensForTokens", () => {
    const token0Amount = ethers.utils.parseEther("5");
    const token1Amount = ethers.utils.parseEther("10");
    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("1662497915624478906");

    beforeEach(async () => {
      await addLiquidity(token0Amount, token1Amount);
      await token0.approve(router.address, ethers.constants.MaxUint256);
    });

    it("happy path", async () => {
      await expect(
        router.swapExactTokensForTokens(
          swapAmount,
          0,
          [token0.address, token1.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: FLAT_FEE }
        )
      )
        .to.emit(token0, "Transfer")
        .withArgs(wallet.address, pair.address, swapAmount)
        .to.emit(token1, "Transfer")
        .withArgs(pair.address, wallet.address, expectedOutputAmount)
        .to.emit(pair, "Sync")
        .withArgs(
          token0Amount.add(swapAmount),
          token1Amount.sub(expectedOutputAmount)
        )
        .to.emit(pair, "Swap")
        .withArgs(
          router.address,
          swapAmount,
          0,
          0,
          expectedOutputAmount,
          wallet.address
        );
    });

    /**
     * Requires router event emitter
    it("amounts", async () => {
      await token0.approve(
        routerEventEmitter.address,
        ethers.constants.MaxUint256
      );
      await expect(
        routerEventEmitter.swapExactTokensForTokens(
          router.address,
          swapAmount,
          0,
          [token0.address, token1.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: FLAT_FEE }
        )
      )
        .to.emit(routerEventEmitter, "Amounts")
        .withArgs([swapAmount, expectedOutputAmount]);
    });
    */

    it("gas", async () => {
      // ensure that setting price{0,1}CumulativeLast for the first time doesn't affect our gas math
      await mineBlock((await ethers.provider.getBlock("latest")).timestamp + 1);
      await pair.sync(overrides);

      await token0.approve(router.address, ethers.constants.MaxUint256);
      await mineBlock((await ethers.provider.getBlock("latest")).timestamp + 1);
      const tx = await router.swapExactTokensForTokens(
        swapAmount,
        0,
        [token0.address, token1.address],
        wallet.address,
        ethers.constants.MaxUint256,
        { ...overrides, value: FLAT_FEE }
      );
      const receipt = await tx.wait();
      expect(receipt.gasUsed).to.eq(160743); // Used to be 101898, 128616
    }).retries(3);

    it("flatFee not supplied", async () => {
      await expect(
        router.swapExactTokensForTokens(
          swapAmount,
          0,
          [token0.address, token1.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: ethers.utils.parseEther("0") }
        )
      ).to.be.reverted;
    });
  });

  describe("swapTokensForExactTokens", () => {
    const token0Amount = ethers.utils.parseEther("5");
    const token1Amount = ethers.utils.parseEther("10");
    const expectedSwapAmount = BigNumber.from("557227237267357629");
    const outputAmount = ethers.utils.parseEther("1");

    beforeEach(async () => {
      await addLiquidity(token0Amount, token1Amount);
    });

    it("happy path", async () => {
      await token0.approve(router.address, ethers.constants.MaxUint256);
      await expect(
        router.swapTokensForExactTokens(
          outputAmount,
          ethers.constants.MaxUint256,
          [token0.address, token1.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: FLAT_FEE }
        )
      )
        .to.emit(token0, "Transfer")
        .withArgs(wallet.address, pair.address, expectedSwapAmount)
        .to.emit(token1, "Transfer")
        .withArgs(pair.address, wallet.address, outputAmount)
        .to.emit(pair, "Sync")
        .withArgs(
          token0Amount.add(expectedSwapAmount),
          token1Amount.sub(outputAmount)
        )
        .to.emit(pair, "Swap")
        .withArgs(
          router.address,
          expectedSwapAmount,
          0,
          0,
          outputAmount,
          wallet.address
        );
    });

    /**
     * Requires router event emitter
    it.skip("amounts", async () => {
      await token0.approve(
        routerEventEmitter.address,
        ethers.constants.MaxUint256
      );
      await expect(
        routerEventEmitter.swapTokensForExactTokens(
          router.address,
          outputAmount,
          ethers.constants.MaxUint256,
          [token0.address, token1.address],
          wallet.address,
          ethers.constants.MaxUint256,
          overrides
        )
      )
        .to.emit(routerEventEmitter, "Amounts")
        .withArgs([expectedSwapAmount, outputAmount]);
    });
    */

    it("flatFee not supplied", async () => {
      await token0.approve(router.address, ethers.constants.MaxUint256);
      await expect(
        router.swapTokensForExactTokens(
          outputAmount,
          ethers.constants.MaxUint256,
          [token0.address, token1.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: ethers.utils.parseEther("0") }
        )
      ).to.be.reverted;
    });
  });

  describe("swapExactETHForTokens", () => {
    const WQPartnerAmount = ethers.utils.parseEther("10");
    const QAmount = ethers.utils.parseEther("5");
    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("1662497915624478906");

    beforeEach(async () => {
      await WQPartner.transfer(WQPair.address, WQPartnerAmount);
      await WQ.deposit({ value: QAmount });
      await WQ.transfer(WQPair.address, QAmount);

      // override the onlyRouter constraint in order to mint tokens
      await registry.setAddress(ROUTER_KEY, wallet.address);
      await WQPair.mint(wallet.address, overrides);
      await registry.setAddress(ROUTER_KEY, router.address);

      await token0.approve(router.address, ethers.constants.MaxUint256);
    });

    it("happy path", async () => {
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapExactETHForTokens(
          0,
          [WQ.address, WQPartner.address],
          wallet.address,
          ethers.constants.MaxUint256,
          {
            ...overrides,
            value: swapAmount.add(FLAT_FEE),
          }
        )
      )
        .to.emit(WQ, "Transfer")
        .withArgs(router.address, WQPair.address, swapAmount)
        .to.emit(WQPartner, "Transfer")
        .withArgs(WQPair.address, wallet.address, expectedOutputAmount)
        .to.emit(WQPair, "Sync")
        .withArgs(
          WQPairToken0 === WQPartner.address
            ? WQPartnerAmount.sub(expectedOutputAmount)
            : QAmount.add(swapAmount),
          WQPairToken0 === WQPartner.address
            ? QAmount.add(swapAmount)
            : WQPartnerAmount.sub(expectedOutputAmount)
        )
        .to.emit(WQPair, "Swap")
        .withArgs(
          router.address,
          WQPairToken0 === WQPartner.address ? 0 : swapAmount,
          WQPairToken0 === WQPartner.address ? swapAmount : 0,
          WQPairToken0 === WQPartner.address ? expectedOutputAmount : 0,
          WQPairToken0 === WQPartner.address ? 0 : expectedOutputAmount,
          wallet.address
        );
    });

    /**
     * Requires router event emitter
    it.skip("amounts", async () => {
      await expect(
        routerEventEmitter.swapExactETHForTokens(
          router.address,
          0,
          [WQ.address, WQPartner.address],
          wallet.address,
          ethers.constants.MaxUint256,
          {
            ...overrides,
            value: swapAmount,
          }
        )
      )
        .to.emit(routerEventEmitter, "Amounts")
        .withArgs([swapAmount, expectedOutputAmount]);
    });
    */

    it("gas", async () => {
      const WQPartnerAmount = ethers.utils.parseEther("10");
      const QAmount = ethers.utils.parseEther("5");
      await WQPartner.transfer(WQPair.address, WQPartnerAmount);
      await WQ.deposit({ value: QAmount });
      await WQ.transfer(WQPair.address, QAmount);

      // override the onlyRouter constraint in order to mint tokens
      await registry.setAddress(ROUTER_KEY, wallet.address);
      await WQPair.mint(wallet.address, overrides);
      await registry.setAddress(ROUTER_KEY, router.address);

      // ensure that setting price{0,1}CumulativeLast for the first time doesn't affect our gas math
      await mineBlock((await ethers.provider.getBlock("latest")).timestamp + 1);
      await pair.sync(overrides);

      const swapAmount = ethers.utils.parseEther("1");
      await mineBlock((await ethers.provider.getBlock("latest")).timestamp + 1);
      const tx = await router.swapExactETHForTokens(
        0,
        [WQ.address, WQPartner.address],
        wallet.address,
        ethers.constants.MaxUint256,
        {
          ...overrides,
          value: swapAmount,
        }
      );
      const receipt = await tx.wait();
      expect(receipt.gasUsed).to.eq(163891); // Used to be 38770, 131726...
    }).retries(3);

    it("flatFee not supplied", async () => {
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapExactETHForTokens(
          expectedOutputAmount,
          [WQ.address, WQPartner.address],
          wallet.address,
          ethers.constants.MaxUint256,
          {
            ...overrides,
            value: swapAmount,
          }
        )
      ).to.be.reverted;
    });
  });

  describe("swapTokensForExactETH", () => {
    const WQPartnerAmount = ethers.utils.parseEther("5");
    const QAmount = ethers.utils.parseEther("10");
    const expectedSwapAmount = BigNumber.from("557227237267357629");
    const outputAmount = ethers.utils.parseEther("1");

    beforeEach(async () => {
      await WQPartner.transfer(WQPair.address, WQPartnerAmount);
      await WQ.deposit({ value: QAmount });
      await WQ.transfer(WQPair.address, QAmount);

      // override the onlyRouter constraint in order to mint tokens
      await registry.setAddress(ROUTER_KEY, wallet.address);
      await WQPair.mint(wallet.address, overrides);
      await registry.setAddress(ROUTER_KEY, router.address);
    });

    it("happy path", async () => {
      await WQPartner.approve(router.address, ethers.constants.MaxUint256);
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapTokensForExactETH(
          outputAmount,
          ethers.constants.MaxUint256,
          [WQPartner.address, WQ.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: FLAT_FEE }
        )
      )
        .to.emit(WQPartner, "Transfer")
        .withArgs(wallet.address, WQPair.address, expectedSwapAmount)
        .to.emit(WQ, "Transfer")
        .withArgs(WQPair.address, router.address, outputAmount)
        .to.emit(WQPair, "Sync")
        .withArgs(
          WQPairToken0 === WQPartner.address
            ? WQPartnerAmount.add(expectedSwapAmount)
            : QAmount.sub(outputAmount),
          WQPairToken0 === WQPartner.address
            ? QAmount.sub(outputAmount)
            : WQPartnerAmount.add(expectedSwapAmount)
        )
        .to.emit(WQPair, "Swap")
        .withArgs(
          router.address,
          WQPairToken0 === WQPartner.address ? expectedSwapAmount : 0,
          WQPairToken0 === WQPartner.address ? 0 : expectedSwapAmount,
          WQPairToken0 === WQPartner.address ? 0 : outputAmount,
          WQPairToken0 === WQPartner.address ? outputAmount : 0,
          router.address
        );
    });

    /**
     * Requires router event emitter
    it.skip("amounts", async () => {
      await WQPartner.approve(
        routerEventEmitter.address,
        ethers.constants.MaxUint256
      );
      await expect(
        routerEventEmitter.swapTokensForExactETH(
          router.address,
          outputAmount,
          ethers.constants.MaxUint256,
          [WQPartner.address, WQ.address],
          wallet.address,
          ethers.constants.MaxUint256,
          overrides
        )
      )
        .to.emit(routerEventEmitter, "Amounts")
        .withArgs([expectedSwapAmount, outputAmount]);
    });
     */

    it("flatFee not supplied", async () => {
      await WQPartner.approve(router.address, ethers.constants.MaxUint256);
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapTokensForExactETH(
          outputAmount,
          ethers.constants.MaxUint256,
          [WQPartner.address, WQ.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: ethers.utils.parseEther("0") }
        )
      ).to.be.reverted;
    });
  });

  describe("swapExactTokensForETH", () => {
    const WQPartnerAmount = ethers.utils.parseEther("5");
    const QAmount = ethers.utils.parseEther("10");
    const swapAmount = ethers.utils.parseEther("1");
    const expectedOutputAmount = BigNumber.from("1662497915624478906");

    beforeEach(async () => {
      await WQPartner.transfer(WQPair.address, WQPartnerAmount);
      await WQ.deposit({ value: QAmount });
      await WQ.transfer(WQPair.address, QAmount);

      // override the onlyRouter constraint in order to mint tokens
      await registry.setAddress(ROUTER_KEY, wallet.address);
      await WQPair.mint(wallet.address, overrides);
      await registry.setAddress(ROUTER_KEY, router.address);
    });

    it("happy path", async () => {
      await WQPartner.approve(router.address, ethers.constants.MaxUint256);
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapExactTokensForETH(
          swapAmount,
          0,
          [WQPartner.address, WQ.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: FLAT_FEE }
        )
      )
        .to.emit(WQPartner, "Transfer")
        .withArgs(wallet.address, WQPair.address, swapAmount)
        .to.emit(WQ, "Transfer")
        .withArgs(WQPair.address, router.address, expectedOutputAmount)
        .to.emit(WQPair, "Sync")
        .withArgs(
          WQPairToken0 === WQPartner.address
            ? WQPartnerAmount.add(swapAmount)
            : QAmount.sub(expectedOutputAmount),
          WQPairToken0 === WQPartner.address
            ? QAmount.sub(expectedOutputAmount)
            : WQPartnerAmount.add(swapAmount)
        )
        .to.emit(WQPair, "Swap")
        .withArgs(
          router.address,
          WQPairToken0 === WQPartner.address ? swapAmount : 0,
          WQPairToken0 === WQPartner.address ? 0 : swapAmount,
          WQPairToken0 === WQPartner.address ? 0 : expectedOutputAmount,
          WQPairToken0 === WQPartner.address ? expectedOutputAmount : 0,
          router.address
        );
    });

    /**
     * Requires router event emitter
    it.skip("amounts", async () => {
      await WQPartner.approve(
        routerEventEmitter.address,
        ethers.constants.MaxUint256
      );
      await expect(
        routerEventEmitter.swapExactTokensForETH(
          router.address,
          swapAmount,
          0,
          [WQPartner.address, WQ.address],
          wallet.address,
          ethers.constants.MaxUint256,
          overrides
        )
      )
        .to.emit(routerEventEmitter, "Amounts")
        .withArgs([swapAmount, expectedOutputAmount]);
    });
    */

    it("flatFee not supplied", async () => {
      await WQPartner.approve(router.address, ethers.constants.MaxUint256);
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapExactTokensForETH(
          swapAmount,
          0,
          [WQPartner.address, WQ.address],
          wallet.address,
          ethers.constants.MaxUint256,
          { ...overrides, value: ethers.utils.parseEther("0") }
        )
      ).to.be.reverted;
    });
  });

  describe("swapETHForExactTokens", () => {
    const WQPartnerAmount = ethers.utils.parseEther("10");
    const QAmount = ethers.utils.parseEther("5");
    const expectedSwapAmount = BigNumber.from("557227237267357629");
    const outputAmount = ethers.utils.parseEther("1");

    beforeEach(async () => {
      await WQPartner.transfer(WQPair.address, WQPartnerAmount);
      await WQ.deposit({ value: QAmount });
      await WQ.transfer(WQPair.address, QAmount);

      // override the onlyRouter constraint in order to mint tokens
      await registry.setAddress(ROUTER_KEY, wallet.address);
      await WQPair.mint(wallet.address, overrides);
      await registry.setAddress(ROUTER_KEY, router.address);
    });

    it("happy path", async () => {
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapETHForExactTokens(
          outputAmount,
          [WQ.address, WQPartner.address],
          wallet.address,
          ethers.constants.MaxUint256,
          {
            ...overrides,
            value: expectedSwapAmount.add(FLAT_FEE),
          }
        )
      )
        .to.emit(WQ, "Transfer")
        .withArgs(router.address, WQPair.address, expectedSwapAmount)
        .to.emit(WQPartner, "Transfer")
        .withArgs(WQPair.address, wallet.address, outputAmount)
        .to.emit(WQPair, "Sync")
        .withArgs(
          WQPairToken0 === WQPartner.address
            ? WQPartnerAmount.sub(outputAmount)
            : QAmount.add(expectedSwapAmount),
          WQPairToken0 === WQPartner.address
            ? QAmount.add(expectedSwapAmount)
            : WQPartnerAmount.sub(outputAmount)
        )
        .to.emit(WQPair, "Swap")
        .withArgs(
          router.address,
          WQPairToken0 === WQPartner.address ? 0 : expectedSwapAmount,
          WQPairToken0 === WQPartner.address ? expectedSwapAmount : 0,
          WQPairToken0 === WQPartner.address ? outputAmount : 0,
          WQPairToken0 === WQPartner.address ? 0 : outputAmount,
          wallet.address
        );
    });

    /**
     * Requires router event emitter
    it.skip("amounts", async () => {
      await expect(
        routerEventEmitter.swapETHForExactTokens(
          router.address,
          outputAmount,
          [WQ.address, WQPartner.address],
          wallet.address,
          ethers.constants.MaxUint256,
          {
            ...overrides,
            value: expectedSwapAmount,
          }
        )
      )
        .to.emit(routerEventEmitter, "Amounts")
        .withArgs([expectedSwapAmount, outputAmount]);
    });
    */

    it("flatFee not supplied", async () => {
      const WQPairToken0 = await WQPair.token0();
      await expect(
        router.swapETHForExactTokens(
          outputAmount,
          [WQ.address, WQPartner.address],
          wallet.address,
          ethers.constants.MaxUint256,
          {
            ...overrides,
            value: expectedSwapAmount,
          }
        )
      ).to.be.reverted;
    });
  });

  describe("isFlatFee", () => {
    it("true", async () => {
      await expect(
        await router.isFlatFee([token0.address, token1.address])
      ).to.eq(true);
    });

    it("false", async () => {
      const pairAddress = await factory.getPair(token0.address, token1.address);
      await factory.toggleFlatFee(pairAddress);
      await expect(
        await router.isFlatFee([token0.address, token1.address])
      ).to.eq(false);
    });
  });
});
