import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

import { ethers } from "hardhat";
import { expect } from "chai";
import { BigNumber, ContractReceipt } from "ethers";
import { ecsign, ecrecover, pubToAddress, bufferToHex } from "ethereumjs-util";

import {
  ContractRegistry,
  DeflatingERC20,
  ERC20Periphery,
  UniswapV2Factory,
  UniswapV2Pair,
  UniswapV2Router02,
  WQ,
} from "../../typechain";
import { getApprovalDigest, MINIMUM_LIQUIDITY } from "./shared/utilities";

const overrides = {
  gasLimit: 9999999,
};

const FLAT_FEE = ethers.utils.parseEther("0.5");
const REVENUE_POOL_KEY = "governance.intApp.QDEX.revenuePool";
const ROUTER_KEY = "governance.intApp.QDEX.router";

const mineBlock = async (secondsInFuture: number) => {
  const latestBlock = await ethers.provider.getBlock("latest");
  await ethers.provider.send("evm_mine", [
    latestBlock.timestamp + secondsInFuture,
  ]);
};

describe("UniswapV2Router02", () => {
  let wallet: SignerWithAddress, other: SignerWithAddress;
  // run "npx hardhat node" and copy the first private key (if it doesn't match this one)
  const privateKey =
    "ac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80";

  let WQ: WQ;
  let WQPartner: ERC20Periphery;
  let token0: ERC20Periphery;
  let token1: ERC20Periphery;
  let factory: UniswapV2Factory;
  let pair: UniswapV2Pair;
  let WQPair: UniswapV2Pair;

  let registry: ContractRegistry;
  let router: UniswapV2Router02;

  beforeEach(async function () {
    [wallet, other] = await ethers.getSigners();

    const registryFactory = await ethers.getContractFactory("ContractRegistry");
    registry = await registryFactory.deploy();
    registry.initialize([wallet.address], [], []);

    const Factory = await ethers.getContractFactory("UniswapV2Factory");
    factory = await Factory.deploy(registry.address);

    let tokenFactory = await ethers.getContractFactory("ERC20Periphery");
    token0 = await tokenFactory.deploy(ethers.utils.parseEther("10000"));
    token1 = await tokenFactory.deploy(ethers.utils.parseEther("10000"));
    WQPartner = await tokenFactory.deploy(ethers.utils.parseEther("10000"));

    const WQFactory = await ethers.getContractFactory("WQ");
    WQ = await WQFactory.deploy();

    await factory.createPair(token0.address, token1.address);
    let pairAddress = await factory.getPair(token0.address, token1.address);
    pair = await ethers.getContractAt("UniswapV2Pair", pairAddress);

    await factory.createPair(WQ.address, WQPartner.address);
    pairAddress = await factory.getPair(WQ.address, WQPartner.address);
    WQPair = await ethers.getContractAt("UniswapV2Pair", pairAddress);

    let routerFactory = await ethers.getContractFactory("UniswapV2Router02");
    router = await routerFactory.deploy(
      factory.address,
      WQ.address,
      registry.address
    );

    // set the router address and let the wallet earn the 0.05% fee
    await registry.setAddress(ROUTER_KEY, router.address);
    await registry.setAddress(REVENUE_POOL_KEY, wallet.address);
  });

  it("factory, WQ", async () => {
    expect(await router.factory()).to.eq(factory.address);
    expect(await router.WQ()).to.eq(WQ.address);
  });

  it("quote", async () => {
    expect(
      await router.quote(
        BigNumber.from(1),
        BigNumber.from(100),
        BigNumber.from(200)
      )
    ).to.eq(BigNumber.from(2));
    expect(
      await router.quote(
        BigNumber.from(2),
        BigNumber.from(200),
        BigNumber.from(100)
      )
    ).to.eq(BigNumber.from(1));
    await expect(
      router.quote(BigNumber.from(0), BigNumber.from(100), BigNumber.from(200))
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_AMOUNT");
    await expect(
      router.quote(BigNumber.from(1), BigNumber.from(0), BigNumber.from(200))
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_LIQUIDITY");
    await expect(
      router.quote(BigNumber.from(1), BigNumber.from(100), BigNumber.from(0))
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_LIQUIDITY");
  });

  it("getAmountOut", async () => {
    expect(
      await router.getAmountOut(
        BigNumber.from(2),
        BigNumber.from(100),
        BigNumber.from(100)
      )
    ).to.eq(BigNumber.from(1));
    await expect(
      router.getAmountOut(
        BigNumber.from(0),
        BigNumber.from(100),
        BigNumber.from(100)
      )
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_INPUT_AMOUNT");
    await expect(
      router.getAmountOut(
        BigNumber.from(2),
        BigNumber.from(0),
        BigNumber.from(100)
      )
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_LIQUIDITY");
    await expect(
      router.getAmountOut(
        BigNumber.from(2),
        BigNumber.from(100),
        BigNumber.from(0)
      )
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_LIQUIDITY");
  });

  it("getAmountIn", async () => {
    expect(
      await router.getAmountIn(
        BigNumber.from(1),
        BigNumber.from(100),
        BigNumber.from(100)
      )
    ).to.eq(BigNumber.from(2));
    await expect(
      router.getAmountIn(
        BigNumber.from(0),
        BigNumber.from(100),
        BigNumber.from(100)
      )
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_OUTPUT_AMOUNT");
    await expect(
      router.getAmountIn(
        BigNumber.from(1),
        BigNumber.from(0),
        BigNumber.from(100)
      )
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_LIQUIDITY");
    await expect(
      router.getAmountIn(
        BigNumber.from(1),
        BigNumber.from(100),
        BigNumber.from(0)
      )
    ).to.be.revertedWith("UniswapV2Library: INSUFFICIENT_LIQUIDITY");
  });

  it("getAmountsOut", async () => {
    await token0.approve(router.address, ethers.constants.MaxUint256);
    await token1.approve(router.address, ethers.constants.MaxUint256);
    await router.addLiquidity(
      token0.address,
      token1.address,
      BigNumber.from(10000),
      BigNumber.from(10000),
      0,
      0,
      wallet.address,
      ethers.constants.MaxUint256,
      overrides
    );

    await expect(
      router.getAmountsOut(BigNumber.from(2), [token0.address])
    ).to.be.revertedWith("UniswapV2Library: INVALID_PATH");
    const path = [token0.address, token1.address];
    expect(await router.getAmountsOut(BigNumber.from(2), path)).to.deep.eq([
      BigNumber.from(2),
      BigNumber.from(1),
    ]);
  });

  it("getAmountsIn", async () => {
    await token0.approve(router.address, ethers.constants.MaxUint256);
    await token1.approve(router.address, ethers.constants.MaxUint256);
    await router.addLiquidity(
      token0.address,
      token1.address,
      BigNumber.from(10000),
      BigNumber.from(10000),
      0,
      0,
      wallet.address,
      ethers.constants.MaxUint256,
      overrides
    );

    await expect(
      router.getAmountsIn(BigNumber.from(1), [token0.address])
    ).to.be.revertedWith("UniswapV2Library: INVALID_PATH");
    const path = [token0.address, token1.address];
    expect(await router.getAmountsIn(BigNumber.from(1), path)).to.deep.eq([
      BigNumber.from(2),
      BigNumber.from(1),
    ]);
  });
});
