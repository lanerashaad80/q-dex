// SPDX-License-Identifier: BUSL-1.1
pragma solidity =0.5.16;

// import "@q-dev/contracts/ContractRegistry.sol";
import "../common/interfaces/IContractRegistry.sol";

import "../common/libraries/Constants.sol";
import "./interfaces/IUniswapV2Factory.sol";
import "./UniswapV2Pair.sol";

// the console can be used to log the `UniswapV2Pair` init code hash
// the init code hash must be updated inside `UniswapV2Library` whenever the code of `UniswapV2Pair` changes
// import "hardhat/console.sol";

contract UniswapV2Factory is IUniswapV2Factory {
    address public registry;
    mapping(address => bool) public toggledOff;

    mapping(address => mapping(address => address)) public getPair;
    address[] public allPairs;

    event PairCreated(
        address indexed token0,
        address indexed token1,
        address pair,
        uint256
    );

    event FlatFeeToggled(address indexed pair, bool);

    modifier onlyDEXParametersVoting() {
        require(
            msg.sender ==
                IContractRegistry(registry).mustGetAddress(
                    Constants.dexParametersVotingKey()
                ),
            "[QDEX-002000]-Permission denied - only governance voting have access."
        );
        _;
    }

    constructor(address _registry) public {
        registry = _registry;
    }

    function allPairsLength() external view returns (uint256) {
        return allPairs.length;
    }

    function createPair(address tokenA, address tokenB)
        external
        returns (address pair)
    {
        require(tokenA != tokenB, "UniswapV2: IDENTICAL_ADDRESSES");
        (address token0, address token1) = tokenA < tokenB
            ? (tokenA, tokenB)
            : (tokenB, tokenA);
        require(token0 != address(0), "UniswapV2: ZERO_ADDRESS");
        require(
            getPair[token0][token1] == address(0),
            "UniswapV2: PAIR_EXISTS"
        ); // single check is sufficient
        bytes memory bytecode = type(UniswapV2Pair).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(token0, token1));
        assembly {
            pair := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }
        IUniswapV2Pair(pair).initialize(token0, token1, registry);
        getPair[token0][token1] = pair;
        getPair[token1][token0] = pair; // populate mapping in the reverse direction
        allPairs.push(pair);
        // console.logBytes32(keccak256(type(UniswapV2Pair).creationCode)); // // Whenever the `UniswapV2Pair` contract changes, the init code hash inside `UniswapV2Library` will need to be updated
        emit PairCreated(token0, token1, pair, allPairs.length);
    }

    /**
     * @notice Toggles flat fee for given pair
     * @param _pair address of pair pool
     */
    function toggleFlatFee(address _pair) external onlyDEXParametersVoting {
        toggledOff[_pair] = !toggledOff[_pair];
        emit FlatFeeToggled(_pair, !toggledOff[_pair]);
    }

    /**
     * @notice Check if flat fee is enabled on given pair pool
     * @param _pair address of pair pool
     * @return true if flat fee is enabled
     */
    function checkForFlatFee(address _pair) public view returns (bool) {
        return !toggledOff[_pair];
    }

    /**
     * @notice Gets init code hash of pair contract
     * @return init code hash of pair contract
     */
    function initHash() public pure returns (bytes32) {
        return keccak256(type(UniswapV2Pair).creationCode);
    }
}
