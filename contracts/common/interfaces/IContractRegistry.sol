pragma solidity >=0.5.0;

interface IContractRegistry {
    function mustGetAddress(string calldata _key)
        external
        view
        returns (address);
}
