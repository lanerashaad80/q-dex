pragma solidity >=0.5.0;

library Constants {
    // Solidity versions before 0.6.2 require getters to retrieve constants from a library
    function revenuePoolKey() internal pure returns (string memory) {
        return "governance.intApp.QDEX.revenuePool";
    }

    function routerKey() internal pure returns (string memory) {
        return "governance.intApp.QDEX.router";
    }

    function dexParametersKey() internal pure returns (string memory) {
        return "governance.experts.EPDR.QDEX.parameters";
    }

    function dexParametersVotingKey() internal pure returns (string memory) {
        return "governance.experts.EPDR.QDEX.parametersVoting";
    }

    function flatFeeKey() internal pure returns (string memory) {
        return "defi.QDEX.flatFee";
    }
}
