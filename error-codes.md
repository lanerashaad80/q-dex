# Onchain Error Codes

Q-DEX revert messages contain a unique error code to allow for unambiguous identification and easier translation to other languages.

The pattern is `<prefix>-<error-number>`

- **prefix**: alphanumeric identifier of the integrated app or code module (3-4 chars)
- **error-number**: 6-9 digits number, to identify a unique error. In the core system we apply an additional pattern which is 3 digits to identify different contracts and 3 digits to identify each error _within that contract_.

Example from the Q-DEX: https://gitlab.com/q-dev/q-dex/-/blob/main/contracts/v2-core/UniswapV2Factory.sol#L35

Code modules and integrated apps should reserve a prefix here to guarantee global uniqueness. But within their prefix they are free to choose error-numbers at their will (6-9 digits, though).

A complete list of all error codes from a module should be provided in the root of the repo as `error-codes.csv` file.

List of all reserved prefixes for error codes can be found: https://gitlab.com/q-dev/system-contracts/-/blob/master/error-codes.md
