# Q-DEX

This repository contains smart contracts for DEX ecosystem on Q blockchain.

## Usage

```sh
$ npm i @q-dev/q-dex
```

### Example

```solidity
pragma solidity 0.8.9;

import "@q-dev/q-dex/contracts/v2-periphery/IWQ.sol";

contract Example {
  function getWQBalance(address _addr) public view returns (uint256 _balance) {
    return IWQ(_addr).balanceOf(_addr);
  }
}

```

## Prepare

Install package dependencies

```sh
$ npm install
```

Compile smart contracts

```sh
$ npx hardhat compile
```

## Deploy

```sh
$ npx hardhat run scripts/deploy.ts --network <network_name>
```

## Run tests

```sh
$ npx hardhat test
```
